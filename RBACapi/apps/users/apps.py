from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'RBACapi.apps.users'
