from django.db import models
from django.contrib.auth.models import AbstractUser
from RBACapi.utils.basemodel import BaseModel

# Create your models here.

'''用户表'''
class User(AbstractUser):
    '''性别'''
    gender_code=(
        (1,"男"),
        (0,"女"),
        (2,"保密")
    )

    mobile =models.CharField(max_length=11,unique=True,verbose_name="手机号")
    nickname = models.CharField(max_length=32, blank=True, null=True, verbose_name="昵称")
    icon =models.ImageField(upload_to='icon')
    gender = models.SmallIntegerField(choices=gender_code,default=1,verbose_name="性别")
    note = models.TextField(null=True,blank=True,verbose_name="备注")
    updated_time = models.DateTimeField(auto_now=True, verbose_name="最后更新时间")

    department = models.ForeignKey(to="permissions.Department",on_delete=models.SET_NULL,db_constraint=False,null=True,blank=True)
    is_admin = models.IntegerField(default=0,verbose_name="是否为管理员")
    is_delete = models.IntegerField(default=0,verbose_name="是否删除")
    create_by = models.ForeignKey(to='self', null=True, on_delete=models.SET_NULL, db_constraint=False,verbose_name='创建人',related_name="create")
    edit_by = models.ForeignKey(to='self',null=True,on_delete=models.SET_NULL,db_constraint=False,verbose_name='编辑者',related_name="edit")
    post = models.ForeignKey(to="Post",null=True,on_delete=models.SET_NULL,db_constraint=False,verbose_name="岗位")

    class Meta:
        db_table = "User"
        verbose_name = "用户表"
        verbose_name_plural = "用户"

    @property
    def gender_name(self):
        return self.get_gender_display()

    def __str__(self):
        return "%s" % self.nickname

    # 拿到所有的菜单信息
    def menu_list(self):
        menu_list = []

        # 根据用户与菜单关系拿数据
        for menu in self.usersmenu.all():

            # 判断父菜单是否为空，判断顶级菜单
            if menu.parent_id is not None:
                parent_id = menu.parent_id.id
            else:
                parent_id = None

            menu_dic = {
                'menuid':menu.id,
                'icon':menu.icon,
                "menuname":menu.menuname,
                'url':menu.compoment,
                'parent_id':parent_id
            }
            menu_list.append(menu_dic)

        # 根据角色拿菜单数据
        for role in self.roles.all():
            for menu in role.menu.all():

                # 判断父菜单是否为空，判断顶级菜单
                if menu.parent_id is not None:
                    parent_id = menu.parent_id.id
                else:
                    parent_id = None

                menu_role_dic = {
                    'menuid': menu.id,
                    'icon': menu.icon,
                    "menuname": menu.menuname,
                    'url': menu.compoment,
                    'parent_id': parent_id
                }
                if menu_role_dic not in menu_list:
                    menu_list.append(menu_role_dic)

        return menu_list


'''岗位表'''
class Post(BaseModel):
    code = models.CharField(max_length=32,verbose_name="岗位编号")
    post_name = models.CharField(max_length=32,verbose_name="岗位名称")

    class Meta:
        db_table = 'Post'