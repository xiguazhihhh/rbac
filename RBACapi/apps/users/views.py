from django.shortcuts import render
from rest_framework.viewsets import GenericViewSet,ModelViewSet
from rest_framework.mixins import ListModelMixin,DestroyModelMixin,CreateModelMixin,UpdateModelMixin,RetrieveModelMixin
from rest_framework.viewsets import ViewSet
from rest_framework.views import APIView
from users.models import User
from utils import PageNumberPagination
from rest_framework.decorators import action
from .serializer import LoginSerializer, UserSerializer, UserAddEditserializer, UserDestroySerializer, \
    ResetPasswordSerializer, UserEditSerializer,UserButtoSaveSerializer
from RBACapi.utils.response import APIResponse

# Create your views here.
'''
get/ -> list
get/1/ -> retrieve
post/ -> create
put/ -> update
delete/ -> destroy
'''


'''
多方式登陆功能
ruleForm: {
        //username和password默认为空
        username: '',
        password: '',
        code: '',
        randomStr: '',
        codeimg: ''
      }
'''

'''多方式登陆接口'''
'''多方式登陆接口'''
class LoginAPIView(ViewSet):

    @action(methods=['POST'], detail=False)
    def login(self, request):
        ser = LoginSerializer(data=request.data)
        if ser.is_valid():
            token = ser.context['token']
            username = ser.context.get("username")
            password = ser.context.get("password")
            button_list = ser.context.get('button_list')
            print(button_list)
            return APIResponse(token=token, username=username,password=password,button_list=button_list)
        else:
            return APIResponse(code=401, msg=ser.errors)

    authentication_classes = []

'''获取所有用户信息'''
class UserAPIView(GenericViewSet,ListModelMixin):
    queryset = User.objects.all().order_by("id")
    serializer_class = UserSerializer

    pagination_class = PageNumberPagination.CommonPageNumberPagination

'''增加、修改用户'''
class UserAddEditAPIView(GenericViewSet,CreateModelMixin,UpdateModelMixin):
    queryset = User.objects.all()
    serializer_class = UserAddEditserializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data,context={'request':request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return APIResponse({"detail":"操作成功"})

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial,context={'request':request})
        ser = serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return APIResponse({"detail":"操作成功"})

'''删除用户'''
class UserDestroyAPIView(GenericViewSet,DestroyModelMixin):
    queryset = User.objects.all()
    serializer_class = UserDestroySerializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_delete = 1
        instance.save()
        return APIResponse({"detail":"操作成功"})

'''重置密码   post请求  userID'''
class ResetPasswordAPIView(ViewSet):

    @action(methods=['POST'],detail=False)
    def pwd(self, request, *args, **kwargs):
        user = User.objects.filter(id=request.data.get('id')).first()
        ser = ResetPasswordSerializer(instance=user,data=request.data)
        ser.is_valid(raise_exception=True)
        if ser.is_valid():
            ser.save()
            return APIResponse({"detail":"修改成功，新密码已通过短信发送"})
        return APIResponse(code=404,msg="密码重置失败，请联系管理员！")

'''用户下线视图类'''
class UserEditAPIView(ViewSet):

    @action(methods=["GET"],detail=False)
    def offonline(self,request):
        username = request.query_params.get('username')
        user = User.objects.filter(username=username).first()
        try:
            user.is_active = False
            user.save()
            print("hhhhh")
            return APIResponse({"detail": "下线成功"})
        except Exception as e:
            return APIResponse({"detail":e})


'''根据用户存储权限'''
class UserButtoSaveAPIView(APIView):

    def post(self,request):
        ser = UserButtoSaveSerializer(data=request.data)
        ser.is_valid(raise_exception=True)
        ser.save()
        return APIResponse({"detail":"配置完毕"})




