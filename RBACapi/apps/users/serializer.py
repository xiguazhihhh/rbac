from rest_framework import serializers
from users.models import User
import re
from django.contrib.auth.hashers import make_password
import random
import string
from rest_framework.exceptions import ValidationError
from rest_framework_jwt.settings import api_settings
from permissions.models import UserRole,Role
from users.models import Post

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


'''登陆序列化类'''
class LoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField()

    class Meta:
        model = User
        fields = [
            'username',
            'password'
        ]

    '''验证用户名及密码，通过则返回token'''
    def validate(self, attrs):
        user = self._get_user(attrs)
        token = self._get_token(user)

        # 获取用户权限列表
        button_list = self._get_button(user)

        self.context['token'] = token
        self.context['password'] = user.password
        self.context['username'] = user.username
        self.context['button_list'] = button_list
        return attrs

    '''获取用户对象'''
    def _get_user(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        '''三种登录方式'''
        if re.match(r'^1[3-9][0-9]{9}$', username):
            user = User.objects.filter(mobile=username).first()
        elif re.match(r'^.+@.+$', username):
            user = User.objects.filter(email=username).first()
        else:
            user = User.objects.filter(username=username).first()

        '''判断用户密码正确性'''
        if user:
            if user.check_password(password):
                return user
            else:
                raise ValidationError('密码错误')
        else:
            raise ValidationError('用户不存在')

    '''获取token并返回'''
    def _get_token(self, user):
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        return token

    '''获取用户权限列表'''
    def _get_button(self,user):
        button_list = []

        # 根据用户取权限
        button_lists  = user.userbuttons.all()
        for button in button_lists:
            button_list.append(button.sys_name)

        # 根据角色取权限
        for role in user.roles.all():
            for button in role.button.all():
                if button.sys_name not in button_list:
                    button_list.append(button.sys_name)

        return button_list

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['post_name']

'''获取所有用户序列化类'''
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        # fields = [
        #     'id', # userId
        #     'username', # userRealName
        #     'nickname', # userName
        #     'password', # userPassword
        #     'date_joined', # addTime
        #     'updated_time', # editTime
        #     'mobile', # userMobile
        #     "gender_name", # userSex
        #     'email', # userEmail
        #     'is_delete',
        #     'is_active', #isLock
        #     'post',
        #     'create_by', # addUser
        #     'edit_by', # editUser
        #     'role', # roleId
        # ]
        fields = "__all__"

    # 子序列化岗位名称
    post = serializers.SerializerMethodField()
    def get_post(self,obj):
        if obj.post == None:
            return "未指定"
        return obj.post.post_name


'''增加、修改用户序列化类'''
class UserAddEditserializer(serializers.ModelSerializer):
    roleID = serializers.ListField(max_length=32)

    class Meta:
        model=User
        fields=[
            'username',
            'nickname',
            'mobile',
            'email',
            "gender",
            'roleID',
            'id'
        ]

    # 重写validate方法
    def validate(self, attrs):
        print(attrs)
        # 分表储存前信息处理
        if attrs.get('id') == None:
            self._perform_save(attrs)
        return attrs

    # 数据更新
    def update(self, instance, validated_data):
        role_list = list(validated_data.pop('roleID'))
        super().update(instance, validated_data)
        # 将用户与角色存储到第三张表
        instance.roles.set(role_list)
        # 存储用户角色表
        return instance

    # 分表储存
    def create(self, validated_data):
        self._check_mobile_phone(validated_data)
        role_list = validated_data.pop('roleID')
        user = User.objects.create_user(**validated_data)

        # 将用户与角色存储到第三张表
        user.roles.add(*role_list)
        return user

    # 添加成功发送信息
    def _send_sms(self):
        pass

    # 检查手机号与邮箱是否存在
    def _check_mobile_phone(self,attrs):
        mobile = attrs['mobile']
        email = attrs['email']

        user1 = User.objects.filter(mobile=mobile).first()
        user2 = User.objects.filter(email=email).first()

        if user1:
            raise ValidationError({'detail':"手机号已存在"})
        elif user2:
            raise ValidationError({'detail':"邮箱已存在"})

    # 数据存储前处理
    def _perform_save(self,attrs):
        phone = attrs.get('mobile')
        password = phone[-6:-1]
        attrs['password'] = password

'''删除用户信息表'''
class UserDestroySerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=["id"]

'''重置密码序列化类'''
class ResetPasswordSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = User
        fields = ['id']

    def validate(self, attrs):  # attrs是字段验证合法之后返回的dict
        # 检验用户id是否存在
        user = self._check_user(attrs)
        # 生成新密码
        new_password = self._set_password(attrs)
        # 发送短信
        self._send_sms(user,new_password)
        return attrs

    # 重写update方法
    def update(self, instance, validated_data):
        password = validated_data['password']
        # 调用django自身的密码生成方法
        instance.password = make_password(password)
        instance.save()
        return instance

    # 检验用户是否存在
    def _check_user(self,attrs):
        userID = attrs.get('id')
        user = User.objects.filter(id=userID).first()
        if not user:
            raise ValidationError({"detail":"用户信息有误，重置失败"})
        return user

    # 生成6位新密码并修改attrs
    def _set_password(self,attrs):
        password = ''
        for i in range(6):
            password = str(password + str(random.randrange(0, 10)))
        attrs['password'] = password
        print(password)
        return password

    # 发送短信
    def _send_sms(self,user,password):
        print(password)

'''用户更新序列化类'''
class UserEditSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    class Meta:
        model=User
        fields=['username']

    # 重写update方法
    def update(self, instance, validated_data):
        instance.is_active = False
        instance.save()
        return instance

'''根据用户存储权限序列化类'''
class UserButtoSaveSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    button_list = serializers.ListField()

    # 重写create方法
    def create(self,validated_data):
        user_id = validated_data['id']
        user = User.objects.filter(id=user_id).first()

        button_list = validated_data['button_list']
        user.userbuttons.set(button_list)
        return user
