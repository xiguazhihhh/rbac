from django.urls import path, include
from rest_framework.routers import SimpleRouter
from .views import LoginAPIView, UserAPIView, UserAddEditAPIView, UserDestroyAPIView, ResetPasswordAPIView, \
    UserEditAPIView,UserButtoSaveAPIView

router = SimpleRouter()
router.register('', LoginAPIView, 'login')
router.register('user_list', UserAPIView, 'user_list')
router.register('add_edit', UserAddEditAPIView, 'add_edit')
router.register('delete', UserDestroyAPIView, 'delete')
router.register('', ResetPasswordAPIView, 'pwd')
router.register('', UserEditAPIView, 'offonline')
print(router.urls)

urlpatterns = [
    path("user_button/",UserButtoSaveAPIView.as_view() ),
    path("", include(router.urls)),
    # path("login/", LoginAPIView.as_view(actions=[{'post': 'login'}])),
]
