from django.db import models
from RBACapi.utils.basemodel import BaseModel

# Create your models here.

'''角色类'''
class Role(BaseModel):
    '''数据权限分类'''
    power_choices = (
        (1,"all"),
        (2,"department"),
        (3,"self"),
        (4,"custom")
    )

    role_name = models.CharField(max_length=32,verbose_name="角色名称")
    role_node = models.CharField(max_length=32,verbose_name="角色代码",null=True)
    is_active = models.BooleanField(default=True,verbose_name="状态")
    create_user = models.ForeignKey(to='users.User',verbose_name="创建人",null=True,on_delete=models.SET_NULL,related_name='createmenus')
    edit_user = models.ForeignKey(to='users.User',verbose_name="最后编辑者",null=True,on_delete=models.SET_NULL,related_name='editmenus')
    user = models.ManyToManyField(to="users.User",through="UserRole",related_name='roles')
    data_power = models.IntegerField(choices=power_choices,default=3,verbose_name="数据权限")

    menu = models.ManyToManyField(to='Menu',through='RoleMenu',related_name='menus')

    class Meta:
        db_table = 'role'

    @property
    def data_dapartments(self):
        return self.departments.filter(is_delete=False,is_active=True).values_list("id",flat=True)

    def data_power_name(self):
        return self.get_data_power_display()

    # 获取编辑者用户名
    def edit_username(self):
        if self.edit_user == None:
            return ""
        return self.edit_user.username

    # 获取创建者用户名
    def create_username(self):
        if self.create_user == None:
            return ""
        return self.create_user.username


class UserRole(BaseModel):
    role = models.ForeignKey(to="Role",to_field='id',on_delete=models.CASCADE)
    user = models.ForeignKey(to='users.User',to_field="id",on_delete=models.CASCADE)

    class Meta:
        db_table = "user_role"
        unique_together = ('role','user',)

class RoleMenu(BaseModel):
    role = models.ForeignKey(to='Role',to_field='id',on_delete=models.CASCADE)
    menu = models.ForeignKey(to='Menu',to_field='id',on_delete=models.CASCADE)

    class Meta:
        db_table = 'role_menu'
        unique_together = ('role','menu',)

    # 返回角色名称
    def role_name(self):
        return self.role.role_name


'''部门表格'''
class Department(BaseModel):
    department_name = models.CharField(max_length=32,verbose_name="部门名称")
    is_active = models.BooleanField(default=True,verbose_name="状态")
    create_user = models.ForeignKey(to='users.User',null=True,blank=True,on_delete=models.CASCADE,db_constraint=False,verbose_name="创建人",related_name="create_user")
    leader = models.ForeignKey(to='users.User',null=True,blank=True,on_delete=models.CASCADE,db_constraint=False,verbose_name="负责人",related_name="leader")

    parent_id = models.ForeignKey(to='self',null=True,verbose_name="上级部门",on_delete=models.SET_NULL,db_constraint=False)

    class Meta:
        db_table = 'department'
        ordering = ['orders']

'''操作权限'''
class Button(BaseModel):

    power_type = models.IntegerField(default=1, verbose_name="数据操作权限")
    button_name = models.CharField(max_length=32, unique=True,default="默认按钮",verbose_name="按钮名称")
    sys_name = models.CharField(max_length=32,unique=True,verbose_name="系统标识")
    url = models.CharField(max_length=32, unique=True, default="待输入",verbose_name="路由")
    edit_user = models.CharField(max_length=32,null=True,blank=True,verbose_name="修改人")
    role = models.ManyToManyField(to='Role',through='ButtonRole',related_name='button',null=True)
    departments = models.ManyToManyField(to='Department',through='ButtonDepartment',related_name='buttons',null=True)
    users_button = models.ManyToManyField(to='users.User',through='UserButton',related_name='userbuttons',null=True)

    class Meta:
        db_table = 'button'


class ButtonRole(BaseModel):
    button = models.ForeignKey(to='Button',on_delete=models.CASCADE,db_constraint=False)
    role = models.ForeignKey(to='Role',on_delete=models.CASCADE,db_constraint=False)

    class Meta:
        unique_together = ('button','role')
        db_table = 'button_role'

class ButtonDepartment(BaseModel):
    button = models.ForeignKey(to='Button', on_delete=models.CASCADE, db_constraint=False)
    departments = models.ForeignKey(to='Department',on_delete=models.CASCADE,db_constraint=False)

    class Meta:
        unique_together = ('button','departments',)
        db_table = 'button_departments'

class UserButton(BaseModel):
    user = models.ForeignKey(to='users.User',on_delete=models.CASCADE,db_constraint=False)
    buttons = models.ForeignKey(to='Button',on_delete=models.CASCADE,db_constraint=False)

    class Meta:
        unique_together = ('user','buttons',)
        db_table = 'user_button'

'''菜单类'''
class Menu(BaseModel):
    menuname = models.CharField(max_length=32,unique=True,verbose_name="菜单名称")
    icon = models.CharField(max_length=64,verbose_name='图标',default="el-icon-delete")
    path = models.CharField(max_length=128,unique=True,verbose_name="菜单url",null=True,blank=True)
    compoment = models.CharField(max_length=128,unique=True,verbose_name="组件路由")
    alwaysShow = models.BooleanField(null=True, blank=True, verbose_name='是否展示')
    hidden = models.BooleanField(default=True, verbose_name='左侧菜单是否展示')
    redirect = models.CharField(max_length=32, null=True, blank=True, verbose_name="面包屑点击")
    is_active = models.BooleanField(default=True, verbose_name='是否启用')

    create_user = models.ForeignKey(to='users.User',blank=True,null=True,on_delete=models.SET_NULL,db_constraint=False,verbose_name='创建人')
    parent_id = models.ForeignKey(to='self',null=True,blank=True,on_delete=models.CASCADE,db_constraint=False)
    users_menu = models.ManyToManyField(to='users.User',through='UserMenu',related_name='usersmenu')

    class Meta:
        ordering = ['orders']
        db_table = 'menu'

    def __str__(self):
        return "%s" % self.menuname

    def create_username(self):
        if self.create_user:
            return self.create_user.username
        else:
            return '初始设置'

    def parent_name(self):
        if self.parent_id:
            return self.parent_id.menuname
        else:
            return "顶级菜单"


class UserMenu(BaseModel):
    users = models.ForeignKey(to='users.User',on_delete=models.CASCADE,db_constraint=False)
    menus = models.ForeignKey(to=Menu,on_delete=models.CASCADE,db_constraint=False)

    class Meta:
        db_table = "user_menu"
        unique_together = ('users','menus',)