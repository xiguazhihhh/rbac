from django.urls import path, include
from .views import MenuAPIView, GetMenuListAPIView, MenuAddAPIView, MenuDetailAPIView, MenuEditAPIView, \
    MenuDeleteAPIView, GetButtonAPIView, EditAddButtonAPIView, DeleteButtonAPIView, GetRoleListAPIView, RoleSaveAPIView, \
    GetRoleMenuAPIView, RoleMenuSaveAPIView, RoleButtonSveAPIView,GetAllButtonAPIView
from rest_framework.routers import SimpleRouter

router = SimpleRouter()
router.register("", MenuAPIView, 'get_menu')
router.register("menu_list", GetMenuListAPIView, 'menu_list')
router.register("button_all_list", GetAllButtonAPIView, 'button_all_list')
router.register("menu_add", MenuAddAPIView, 'menu_add')
router.register("menu_detail", MenuDetailAPIView, 'menu_detail')
router.register("menu_edit", MenuEditAPIView, 'menu_edit')
router.register("menu_delete", MenuDeleteAPIView, 'menu_delete')
router.register("button_list", GetButtonAPIView, 'button_list')
router.register("", EditAddButtonAPIView, 'button_save')
router.register("button_delete", DeleteButtonAPIView, 'button_delete')
router.register("get_role", GetRoleListAPIView, 'get_role')
router.register("", RoleSaveAPIView, 'role_save')
router.register("get_role_menu", GetRoleMenuAPIView, 'get_role_menu')
router.register("role_menu_save", RoleMenuSaveAPIView, 'role_menu_save')
print(router.urls)

urlpatterns = [
    path("role_button_save/", RoleButtonSveAPIView.as_view()),
    path("", include(router.urls))

]
