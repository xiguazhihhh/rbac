from .models import Menu
from users.models import User
from .models import Menu,Button,Role,RoleMenu
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

'''菜单序列化类'''
class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = []


'''获取用户菜单序列化类'''
class UserMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            "username",
            'menu_list'
        ]


class MenuChildSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = ('menuname')


'''获取所有菜单序列化类'''
class GetMenuListSerializer(serializers.ModelSerializer):
    create_username = serializers.CharField()

    class Meta:
        model = Menu
        fields = [
            'id',
            'menuname',
            'path',
            'compoment',
            'is_active',
            'parent_id',
            'create_username',
            "parent_name",
            "is_delete",
            "icon",
            "alwaysShow",
            "hidden",
            "create_user",
        ]

        parent_id = MenuChildSerializer()


'''菜单添加序列化类'''
class MenuAddSerializer(serializers.ModelSerializer):
    # 需要重写两个外键的字段检验
    create_username = serializers.CharField()
    parent_id = serializers.CharField()

    class Meta:
        model = Menu
        fields = [
            'menuname',
            'icon',
            'path',
            'compoment',
            'alwaysShow',
            'hidden',
            'is_active',
            'parent_id',
            "create_username"
        ]

    def create(self, validated_data):
        # 拿到关联用户对象
        username = validated_data.pop('create_username')
        user = User.objects.filter(username=username).first()

        # 拿到关联菜单对象
        parent_name = validated_data["parent_id"]
        parent_menu = Menu.objects.filter(menuname=parent_name).first()

        # 将两个外键字段分别赋值queryset对象
        validated_data['create_user'] = user
        validated_data["parent_id"] = parent_menu

        # 返回menu对象
        menu = Menu.objects.create(**validated_data)
        return menu

'''获取单条菜单详细信息序列化类'''
class MenuDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = [
            "create_username", # model重写字段
            "parent_name",  # model重写字段
            "id",
            "is_delete",
            "orders",
            "menuname",
            "icon",
            "path",
            "compoment",
            "alwaysShow",
            "hidden",
            "redirect",
            "is_active",
            "create_user",
            "parent_id",
            "users_menu"
        ]

'''菜单修改序列化类'''
class MenuEditSerializer(serializers.ModelSerializer):
    create_username = serializers.CharField()
    parent_name = serializers.CharField()

    class Meta:
        model = Menu
        fields = [
            "create_username",
            "parent_name",
            "id",
            "is_delete",
            "menuname",
            "icon",
            "path",
            "compoment",
            "alwaysShow",
            "hidden",
            "is_active",
        ]

    def update(self, instance, validated_data):

        # 拿到创建者用户对象
        user = self._get_create_user(validated_data)

        # 拿到父组件对象
        parent_menu = self._get_father_queryset(validated_data)

        if user and parent_menu:
            validated_data['create_user'] = user
            validated_data['parent_id'] = parent_menu
            instance = super().update(instance, validated_data)

        elif not user:
            raise ValueError({'detail':"创建者信息不存在"})
        elif not parent_menu:
            raise ValueError({"detail":"父组件不存在"})

        return instance

    # 拿到创建者用户对象
    def _get_create_user(self,validated_data):
        username = validated_data['create_username']
        if username != "初始设置":
            user = User.objects.filter(username=username).first()
        else:
            user = User.objects.filter(username="zcc").first()

        return user

    # 拿到父组件对象
    def _get_father_queryset(self,validated_data):
        parent_name = validated_data['parent_name']
        if parent_name != "顶级菜单":
            parent_menu = Menu.objects.filter(menuname=parent_name).first()
        else:
            parent_menu = None
        return parent_menu

'''删除菜单序列化类'''
class MenuDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = [
            'id'
        ]

    def update(self, instance, validated_data):
        instance.is_delete = validated_data['is_delete']
        instance = super().update(instance, validated_data)
        return instance


'''获取所有权限序列化类'''
class GetButtonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Button
        fields = "__all__"

'''编辑权限序列化类'''
class EditButtonSerializer(serializers.ModelSerializer):
    '''
    button_name: "用户新增"
    edit_user: "超级管理员"
    id: 1
    is_delete: true
    power_type: 1
    url: "api/users/add_edit/"
    '''
    class Meta:
        model=Button
        fields = "__all__"


'''新增权限序列化类'''
class AddButtonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Button
        fields = [
            'is_delete',
            "power_type",
            "button_name",
            "url",
            "edit_user"
        ]


'''删除权限序列化类'''
class DeleteButtonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Button
        fields =  [
            'is_delete',
        ]


'''获取角色列表序列化类'''
class GetRoleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = [
            "create_username",
            'edit_username',
            'created_time',
            'updated_time',
            'id',
            'role_name',
            'role_node',
            'is_active',
            'is_delete'
        ]


'''角色修改-添加序列化类'''
class RoleSaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = [
            'role_node',
            'role_name',
            'is_active',
            'is_delete',
        ]

    def update(self, instance, validated_data):

        # 获取请求用户信息
        edit_user = self._get_user()
        # 存储前数据处理
        validated_data = self._select_data(edit_user,validated_data)
        # 存储数据
        role = super().update(instance,validated_data)
        return role

    # 新建数据
    def create(self, validated_data):
        create_user = self.context.get('create_user')
        validated_data['create_user'] = create_user
        role = Role.objects.create(**validated_data)
        return role

    # 获取用户信息
    def _get_user(self):
        user = self.context.get('edit_user')
        return user

    # 更新前数据处理
    def _select_data(self,edit_user,validated_data):
        validated_data['edit_user'] = edit_user
        return validated_data


'''根据角色获取菜单列表'''
class GetRoleMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model= Role
        fields = [
            'id',
            'role_name',
            'menulist'
        ]

    menulist = serializers.SerializerMethodField()
    def get_menulist(self,obj):
        return [obj.menu.all().values()]

'''根据角色存储菜单'''
class RoleMenuSaveSerializerc(serializers.ModelSerializer):
    menu = serializers.ListField()
    id= serializers.IntegerField()
    class Meta:
        model = Role
        fields = [
            'id',
            'menu'
        ]

    # 重写create方法
    def create(self, validated_data):
        role = Role.objects.filter(id=validated_data['id']).first()
        role.menu.set(validated_data['menu'])
        return role


'''根据角色存储权限'''
class RoleButtonSaveSerializer(serializers.Serializer):

    roleId = serializers.IntegerField()
    permissionIds = serializers.ListField()

    # 重写create方法，跨表存储
    def create(self, validated_data):
        role_id = validated_data['roleId']
        role = Role.objects.filter(id=role_id).first()
        role.button.set(validated_data['permissionIds'])
        return role

