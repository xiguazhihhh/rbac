# Generated by Django 2.2.20 on 2021-08-16 10:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('permissions', '0013_auto_20210816_1632'),
    ]

    operations = [
        migrations.AlterField(
            model_name='role',
            name='create_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='createmenus', to='users.User', verbose_name='创建人'),
        ),
        migrations.AlterField(
            model_name='role',
            name='edit_user',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='editmenus', to='users.User', verbose_name='最后编辑者'),
        ),
    ]
