from django.shortcuts import render
from rest_framework.viewsets import GenericViewSet,ViewSet
from rest_framework.mixins import ListModelMixin
from rest_framework.decorators import action
from RBACapi.utils.response import APIResponse
from RBACapi.utils.token_to_user import token_to_user
from rest_framework.views import APIView
from users.models import User
from .models import Menu,Button,Role
from .serializer import UserMenuSerializer, GetMenuListSerializer, MenuAddSerializer, MenuEditSerializer, \
    MenuDetailSerializer, MenuDeleteSerializer, GetButtonSerializer, EditButtonSerializer, AddButtonSerializer, \
    DeleteButtonSerializer, GetRoleListSerializer, RoleSaveSerializer, GetRoleMenuSerializer, RoleMenuSaveSerializerc, \
    RoleButtonSaveSerializer
from rest_framework_jwt.settings import api_settings
from RBACapi.utils.relationtree import Tree
from rest_framework.mixins import CreateModelMixin,UpdateModelMixin,RetrieveModelMixin,DestroyModelMixin
from rest_framework.filters import SearchFilter
from utils import PageNumberPagination

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

# Create your views here.


'''获取菜单列表视图类'''
class MenuAPIView(ViewSet):

    # 获取菜单，get请求
    @action(methods=['Get'],detail=False)
    def get_menu(self,request):
        token = request.query_params.get('token')
        user = self._get_userinfo(token)
        res = UserMenuSerializer(instance=user)

        # 测试数据
        # l = [
        #     {'menuid':1,"menuname":"hhhh",'parent_id':None,'url':2222,'icon':1111},
        #     {'menuid':2,"menuname":"xxx",'parent_id':None,'url':2222,'icon':1111},
        #     {'menuid':3,"menuname":"aaaa",'parent_id':1,'url':2222,'icon':1111},
        #     {'menuid': 4, "menuname": "bbb", 'parent_id': 2,'url':2222,'icon':1111},
        #     {'menuid': 5, "menuname": "ccc", 'parent_id': 3,'url':2222,'icon':1111}
        # ]

        # 调用Tree方法，生成菜单树
        tree_list = Tree(res.data['menu_list'])
        data = tree_list.build_tree()
        return APIResponse(data=data)

    # 根据token获得用户queryset
    def _get_userinfo(self,token):
        token = token.encode()
        user = token_to_user(token)
        return user

'''获取所有菜单列表视图类'''
class GetMenuListAPIView(GenericViewSet,ListModelMixin):
    queryset = Menu.objects.all()
    serializer_class = GetMenuListSerializer

'''添加菜单视图类'''
class MenuAddAPIView(GenericViewSet,CreateModelMixin):
    queryset = Menu.objects.all()
    serializer_class = MenuAddSerializer

    # 因为前端传来的数据带有不包含字段，不能serializer.data，重写create
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return APIResponse({"detail":"成功"})

'''获取单条菜单信息视图类'''
class MenuDetailAPIView(GenericViewSet,RetrieveModelMixin):
    queryset = Menu.objects.all()
    serializer_class = MenuDetailSerializer

'''修改单条菜单视图类'''
class MenuEditAPIView(GenericViewSet,UpdateModelMixin):
    queryset = Menu.objects.all()
    serializer_class = MenuEditSerializer

'''删除单条菜单视图类'''
class MenuDeleteAPIView(GenericViewSet,DestroyModelMixin):
    queryset = Menu.objects.all()
    serializer_class = MenuDeleteSerializer

    # 重写删除方法
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_delete = True
        instance.save()
        return APIResponse({'detail':"成功删除"})


'''获取权限列表视图类'''
class GetButtonAPIView(GenericViewSet,ListModelMixin):
    queryset = Button.objects.all().order_by("id")
    serializer_class = GetButtonSerializer

    filter_backends = [SearchFilter, ]
    search_fields = ['button_name', ]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return APIResponse(data=serializer.data)

'''获取权限列表视图类'''
class GetAllButtonAPIView(GenericViewSet,ListModelMixin):
    queryset = Button.objects.all()
    serializer_class = GetButtonSerializer

    pagination_class = PageNumberPagination.CommonPageNumberPagination

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return APIResponse(data=serializer.data)

'''编辑权限列表视图类'''
class EditAddButtonAPIView(ViewSet):

    @action(methods=['POST'],detail=False)
    def button_save(self,request):
        button_id = request.data.get("id")
        # 编辑
        if button_id:
            request.data.pop("id")
            button = Button.objects.filter(id=button_id).first()
            ser = EditButtonSerializer(instance=button,data=request.data)
            ser.is_valid(raise_exception=True)
            ser.save()
            return APIResponse({"datail":"修改成功"})
        # 新增
        else:
            serializer = AddButtonSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return APIResponse(data=serializer.data)

'''删除单个权限视图类'''
class DeleteButtonAPIView(GenericViewSet,UpdateModelMixin):
    queryset = Button
    serializer_class = DeleteButtonSerializer


'''获取所有角色列表视图类'''
class GetRoleListAPIView(GenericViewSet,ListModelMixin):
    queryset = Role.objects.all()
    serializer_class = GetRoleListSerializer

'''角色存储视图类'''
class RoleSaveAPIView(ViewSet):

    @action(methods=['post'],detail=False)
    def role_save(self,request):
        role_id = request.data.get('id')
        token = request.data.get('token')
        edit_user = token_to_user(token)

        # 编辑
        if role_id:
            role = Role.objects.filter(id=role_id).first()
            request.data.pop('token')
            request.data.pop('id')
            ser = RoleSaveSerializer(instance=role,data=request.data,context={'edit_user':edit_user})
            ser.is_valid(raise_exception=True)
            ser.save()
            return APIResponse({'detail':'编辑成功'})

        # 添加
        else:
            # 删除多余数据
            request.data.pop('id')
            token = request.data.pop('token')
            create_user = token_to_user(token)
            ser = RoleSaveSerializer(data=request.data,context={'create_user':create_user})
            ser.is_valid(raise_exception=True)
            ser.save()
            return APIResponse({"'detail":"添加成功"})


'''根据角色拿到所有的菜单权限'''
class GetRoleMenuAPIView(GenericViewSet,RetrieveModelMixin):
    queryset = Role.objects.all()
    serializer_class = GetRoleMenuSerializer


'''根据角色存储菜单'''
class RoleMenuSaveAPIView(GenericViewSet,CreateModelMixin):
    queryset = Role.objects.all()
    serializer_class = RoleMenuSaveSerializerc

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return APIResponse({'detail':"配置完成"})


'''根据角色存储权限'''
class RoleButtonSveAPIView(APIView):

    def post(self,request):
        print(0000000)
        print(request.data)
        ser = RoleButtonSaveSerializer(data=request.data)
        ser.is_valid(raise_exception=True)
        ser.save()
        return APIResponse({"detail":"配置成功"})



