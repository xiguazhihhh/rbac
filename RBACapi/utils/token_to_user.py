from rest_framework_jwt.settings import api_settings
from users.models import User

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER


# token转用户queryset
def token_to_user(token):
    user = jwt_decode_handler(token)
    user_id = user['user_id']
    user = User.objects.filter(id=user_id).first()
    return user
