'''获取关系树'''
class Tree:
    def __init__(self, data):
        self.data = data
        self.root_node = list()
        self.common_node = dict()
        self.tree = list()

    def find_root_node(self) -> list:
        """
        查找根节点
        :param data:原始数据
        :return:根节点
        """
        for node in self.data:
            # 假定father_id是None就是根节点
            # 例如有些数据库设计会直接把根节点标识出来。
            if node["parent_id"] is None:
                self.root_node.append(node)
        return self.root_node

    def find_common_node(self) -> dict:
        """
        寻找共同的父节点
        :param data: 原始数据
        :return: 共同的父节点字典
        """
        for node in self.data:
            parent_id = node.get("parent_id")
            # 排除根节点情况
            if parent_id is not None:
                # 如果父节点ID不在字典中则添加到字典中
                if parent_id not in self.common_node:
                    self.common_node[parent_id] = list()
                self.common_node[parent_id].append(node)
        return self.common_node

    def build_tree(self) -> list:
        """
        生成目录树
        :param root_node: 根节点列表
        :param common_node: 共同父节点字典
        :return:
        """
        self.find_root_node()
        self.find_common_node()
        for root in self.root_node:
            # 生成字典
            base = dict(menuid=root["menuid"],menuname=root["menuname"],icon=root['icon'],url=root["url"],menus=list())
            print(base)
            # 遍历查询子节点
            self.find_child(base["menuid"], base["menus"])
            # 添加到列表
            self.tree.append(base)
        return self.tree

    def find_child(self, parent_id: int, child_node: list):
        """
        查找子节点
        :param father_id:父级ID
        :param child_node: 父级孩子节点
        :param common_node: 共同父节点字典
        :return:
        """
        # 获取共同父节点字典的子节点数据
        child_list = self.common_node.get(parent_id, [])
        for item in child_list:
            # 生成字典
            base = dict(menuid=item["menuid"],menuname=item["menuname"],icon=item["icon"], url=item["url"], menus=list())
            # 遍历查询子节点
            self.find_child(item["menuid"], base["menus"])
            # 添加到列表
            child_node.append(base)
