from rest_framework.pagination import PageNumberPagination
class CommonPageNumberPagination(PageNumberPagination):
    page_size = 10  # 每页显示条数
    page_query_param = 'page'  # 查询时用的参数http://127.0.0.1:8000/books/?page=2
    page_size_query_param = 'size'  # http://127.0.0.1:8000/books/?page=1&size=4  更改返回条数，第一页返回四条
    max_page_size = 10  # 一页最多显示10条