import redis
from celery import Celery

backend = 'redis://127.0.0.1:6379/1'
broker = 'redis://127.0.0.1:6379/2'
app = Celery('test',broker=broker,backend=backend,include=[
    'scripts.test_cache',
])